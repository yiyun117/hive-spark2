package hivespark2

import com.hortonworks.hwc.HiveWarehouseSession
import org.apache.spark.sql.SparkSession
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.sql.Row


object UserAnime extends App {
    if (args.length == 0) {
        throw new Exception("Please define the source: hive or a csv file path")
    }
    val source =args(0)
    val spark = SparkSession
      .builder()
      .appName("User anime exercise by yiyun")
      .enableHiveSupport()
      .getOrCreate()
    val hive = HiveWarehouseSession.session(spark).build()
    val customSchema = StructType(Array(
        StructField("username", StringType, true),
        StructField("anime_id", IntegerType, true),
        StructField("my_watched_episodes", IntegerType, true),
        StructField("my_start_date", TimestampType, true),
        StructField("my_finish_date", TimestampType, true),
        StructField("my_score", IntegerType, true),
        StructField("my_status", IntegerType, true),
        StructField("my_rewatching", IntegerType, true),
        StructField("my_rewatching_ep", IntegerType, true),
        StructField("my_last_updated", IntegerType, true),
        StructField("my_tags", IntegerType, true))
    )
    var df = spark.createDataFrame(spark.sparkContext.emptyRDD[Row], customSchema)
    if (source =="hive") {
        hive.setDatabase("ychen")
        df = hive.execute("select * from useranime_bucket where anime_id is not null")
    } else {
        df = spark.read.format("csv").schema(customSchema).option("header", "true").load(source)
    }

    //1.utilisateurs différents
    val distinctUser = df.select(df("username")).distinct()
    println("Distinct user Count: " + distinctUser.count())

    //2. Pour chaque utilisateur, le nombre d'Animés différents qu'il a regardé
    val userAnimeCount = df.groupBy("username").agg(countDistinct("anime_id") as "TotalNumberAnimes")
    userAnimeCount.show(2)

    //3. Pour chaque utilisateur, l'Animé qu'il a le plus regardé
    val w = Window.partitionBy("username").orderBy(desc("my_watched_episodes"))
    val mostWatchedByUser = df.withColumn("rn", row_number.over(w)).where(col("rn") === 1).drop("rn")
    mostWatchedByUser.show(2)

    //5. L'utilisateur qui a regardé le plus d’Animés
    val userWatchedMostAnimes = df.groupBy("username").agg(sum("anime_id") as "sum").orderBy(desc("sum"))
    userWatchedMostAnimes.show(1)

    //6. L'utilisateur qui a regardé le plus d’épisodes
    val userWatchedMostEpisodes = df.groupBy("username").agg(sum("my_watched_episodes") as "sum").orderBy(desc("sum"))
    userWatchedMostEpisodes.show(1)

    //7. Les 100 utilisateurs qui ont donné les moyennes de notes les plus élevées.
    val bestScore = df.groupBy("username").agg(avg("my_score") as "avgScore").orderBy(desc("avgScore"))
    bestScore.show(100)
    hive.close()
    spark.stop()

}
