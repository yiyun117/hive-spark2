import org.apache.spark.sql.SparkSession
import com.hortonworks.hwc.HiveWarehouseSession
import com.hortonworks.hwc.HiveWarehouseSession._
import org.apache.spark.sql.functions.countDistinct
import org.apache.spark.sql.expressions.Window

object userAnime {
  val hive = HiveWarehouseSession.session(spark).build
  hive.setDatabase("ychen")
  lazy val df = hive.excute("select * from useranime_bucket200_username where anime_id is not null")

  //1.utilisateurs différents
  val distinctUser = df.select(df("username")).distinct
  println(distinctUser.count())

  //2. Pour chaque utilisateur, le nombre d'Animés différents qu'il a regardé
  val userAnimeCount = df.groupBy("username").agg(countDistinct("anime_id"))
  userAnimeCount.head().show()

  //3. Pour chaque utilisateur, l'Animé qu'il a le plus regardé
  val w = Window.partitionBy("username").orderBy("my_watched_episodes".desc)
  val mostWatchedByUser = df.withColumn("rn", row_number.over(w)).where("rn" === 1).drop("rn")
  mostWatchedByUser.head().show()

  //5. L'utilisateur qui a regardé le plus d’Animés
  val userWatchedMostAnimes = df.groupBy("uername").agg(sum("anime_id") as "sum").orderBy("sum")
  userWatchedMostAnimes.first().show()

  //6. L'utilisateur qui a regardé le plus d’épisodes
  val userWatchedMostEpisodes = df.groupBy("uername").agg(sum("my_watched_episodes") as "sum").orderBy("sum")
  userWatchedMostAnimes.first().show()

  //7. Les 100 utilisateurs qui ont donné les moyennes de notes les plus élevées.
  val bestScore = df.groupBy("uername").agg(avg("my_score") as "avgScore").orderBy("avgScore")
  bestScore.head(100).show()
}
